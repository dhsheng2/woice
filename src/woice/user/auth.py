from django.db import transaction

from ..models.user import User, UserOAuth


def authenticate_with_oauth_vendor(vendor, union_id, **kwargs):

    try:
        oauth_entry = UserOAuth.objects.filter(
            vendor=vendor,
            union_id=union_id).get()

    except UserOAuth.DoesNotExist:
        user = User.objects.create_user(
            **{'nickname': kwargs.get('nickname', '')}
        )
        oauth_entry = UserOAuth(
            vendor=vendor,
            union_id=union_id
        )
        oauth_entry.set_options(kwargs)

        with transaction.atomic():
            user.save()
            oauth_entry.user = user
            oauth_entry.save()

    return oauth_entry
