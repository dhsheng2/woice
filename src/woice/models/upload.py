from django.db import models

from .user import User


class Transaction(models.Model):

    TYPE_ENUM_AVATAR = 'avatar'
    TYPE_ENUM_RHYTHM = 'rhythm'

    TYPE_ENUM = (
        (TYPE_ENUM_AVATAR, TYPE_ENUM_AVATAR),
        (TYPE_ENUM_RHYTHM, TYPE_ENUM_RHYTHM),
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    type = models.CharField(max_length=10, choices=TYPE_ENUM, db_index=True)

    size = models.IntegerField()  # byte unit

    success = models.BooleanField(default=0)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    options = models.TextField()

    class Meta:
        db_table = 'upload_transactions'
