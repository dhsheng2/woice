import json

from django.db import models
from django.contrib.auth.models import BaseUserManager
from django.contrib.auth.models import AbstractBaseUser

from .base import OptionsMixin


class UserManager(BaseUserManager):

    def create_user(self, email='', password='', **kwargs):
        user = self.model(email=email)
        if password:
            user.set_password(password)
        for k, v in kwargs.items():
            setattr(user, k, v)
        user.save()
        return user

    def create_superuser(self, email, password, **kwargs):
        return self.create_user(email, password, **kwargs)


class User(AbstractBaseUser, OptionsMixin):

    objects = UserManager()

    USERNAME_FIELD = 'id'
    REQUIRED_FIELDS = []

    email = models.CharField(max_length=60, default='', null=False)
    nickname = models.CharField(max_length=100, default='', null=False)
    created = models.DateTimeField(auto_now_add=True, null=False)
    updated = models.DateTimeField(auto_now=True, null=False)
    options = models.TextField(default='{}', null=False)

    def get_salt(self):
        return 'salt'

    def is_admin_role(self):
        return False

    def __str__(self):
        return str(self.pk)

    class Meta:
        db_table = 'users'

    def is_super_user(self):
        return self.role == User.ROLE_ADMIN

    def has_perm(self, perm, obj=None):
        return self.is_super_user()

    def has_module_perms(self, app_label):
        return self.is_super_user()

    def get_short_name(self):
        return self.name

    def is_staff(self):
        return True

    @property
    def name(self):
        return self.nickname or self.email.split('@')[0]


class UserOAuth(models.Model, OptionsMixin):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    vendor = models.CharField(max_length=20)
    union_id = models.CharField(max_length=64, unique=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    options = models.CharField(max_length=1000)  # json

    class Meta:
        db_table = 'users_oauth'


class UserCounter(models.Model):
    artwork_score = models.IntegerField()
    guess_score = models.IntegerField()
    user = models.ForeignKey(User,  on_delete=models.CASCADE)

    class Meta:
        db_table = 'users_counter'
