from django.db import models

from .user import User


class Rhythm(models.Model):

    PRIVILEGE_ENUM_PRIVATE = 0
    PRIVILEGE_ENUM_FRIEND = 1
    PRIVILEGE_ENUM_PUBLIC = 2
    PRIVILEGE_ENUM_SPECIAL = 3

    PRIVILEGE_ENUM = (
        (PRIVILEGE_ENUM_PRIVATE, PRIVILEGE_ENUM_PRIVATE),
        (PRIVILEGE_ENUM_FRIEND, PRIVILEGE_ENUM_FRIEND),
        (PRIVILEGE_ENUM_PUBLIC, PRIVILEGE_ENUM_PUBLIC),
        (PRIVILEGE_ENUM_SPECIAL, PRIVILEGE_ENUM_SPECIAL),
    )

    name = models.CharField(max_length=255)
    original_name = models.CharField(max_length=255)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    file = models.URLField()
    score = models.IntegerField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    options = models.TextField()

    class Meta:
        db_table = 'artworks_rhythm'


class RhythmScore(models.Model):
    rhythm = models.ForeignKey(Rhythm, on_delete=models.CASCADE)
    score = models.IntegerField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'artworks_rhythm_score'
