import json


class OptionsMixin(object):
    def set_options(self, options):
        try:
            options = json.dumps(options)
        except (TypeError, ValueError):
            options = '{}'
        setattr(self, 'options', options)
