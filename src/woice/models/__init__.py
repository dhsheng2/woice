import pkgutil

from django.db import models


def load_models(context, root_module, path):
    for loader, module_name, is_pkg in pkgutil.walk_packages(path, root_module + '.'):
        module = __import__(module_name, globals(), locals(), ['__name__'])
        for attr in dir(module):
            if not attr.startswith('-'):
                o = getattr(module, attr)
                if isinstance(o, (type, )) and issubclass(o, models.Model):
                    context[module_name] = module

load_models(globals(), __name__, __path__)
