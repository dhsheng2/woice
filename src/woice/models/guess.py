from django.db import models

from .artworks import Rhythm
from .user import User


class GuessRhythm(models.Model):

    artworks_rhythm = models.ForeignKey(Rhythm, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    guess_name = models.CharField(max_length=255)
    score = models.IntegerField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'guess_rhythms'
