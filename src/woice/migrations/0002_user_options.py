# Generated by Django 2.0 on 2017-12-17 14:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('woice', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='options',
            field=models.TextField(default='{}'),
        ),
    ]
