from django.contrib import admin
from django.conf.urls import url, include

from .api import urls as api_urls

urlpatterns = [
    url('api/', include(api_urls)),
    url('admin/', admin.site.urls),
]
