from logging import getLogger


__all__ = [
    'get_api_logger',
]


def get_api_logger():
    return getLogger('api')