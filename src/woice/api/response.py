import time

from django.http.response import JsonResponse


class APIJsonResponse(JsonResponse):

    def __init__(self, data=None, errno=0, error=None, **kwargs):
        data = data or {}
        if not isinstance(data, dict):
            raise ValueError('Data is not dict')

        data = {
            'timestamp': int(time.time()),
            'errno': errno or 0,
            'error': error or '',
            'data': data or {}
        }

        super(APIJsonResponse, self).__init__(
            data=data,
            json_dumps_params=kwargs.pop('json_dump_params', {}),
            **kwargs
        )
