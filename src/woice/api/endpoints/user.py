from ..base import APIView
from ..decorators.parameters import verify
from ..user.token import generate_user_token
from ...user.auth import authenticate_with_oauth_vendor


class Login(APIView):

    @verify({
        'email': {'type': 'string', 'message': '邮箱地址错误'},
        'password': {'type': 'string', 'message': '密码错误'}
    })
    def post(self, request):
        pass


class Logout(APIView):

    def post(self, request):
        pass


class RegisterWithEmail(APIView):

    def post(self, request):
        pass


class RegisterWithPhone(APIView):
    pass


class LoginWithEmailOrPhone(APIView):

    @verify({
        'account': {'type': 'string', 'message': '请输入手机号或邮箱'},
        'password': {'type': 'string', 'message': '密码错误'}
    })
    def post(self, request):
        pass


class LoginWithOAuth(APIView):

    @verify({
        'vendor': {'type': 'string', 'message': 'Invalid vendor'},
        'union_id': {'type': 'string', 'message': 'Invalid union id'}
    })
    def post(self, request):
        user_oauth = authenticate_with_oauth_vendor(
            request.POST.get('vendor'),
            request.POST.get('union_id'),
            **{'nickname': request.POST.get('nickname', '')}
        )

        user = user_oauth.user
        response = self.render(data={
            'user_id': user.pk,
            'nickname': user.nickname,
        })
        response['X-Authorize-Token'] = generate_user_token(user_oauth.user)
        return response
