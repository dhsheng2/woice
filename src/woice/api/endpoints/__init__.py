from ..base import APIView


class Ping(APIView):

    def get(self, request):
        data = {
            'headers': {},
            'post': {},
            'get': {}
        }
        for k, v in request.META.items():
            if k.isupper():
                data['headers'][k] = v
        for k, v in request.GET.items():
            data['get'][k] = v

        for k, v in request.POST.items():
            data['post'][k] = v

        return self.render(data)


class Config(APIView):

    def get(self, request):
        pass
