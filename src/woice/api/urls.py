from django.conf.urls import url

from .endpoints import Ping, Config
from .endpoints.user import Login, LoginWithOAuth


urlpatterns = [
    url('ping$', Ping.as_view()),
    url('config$', Config.as_view()),
    url('user/login$', Login.as_view()),
    url('user/login/oauth$', LoginWithOAuth.as_view())
]
