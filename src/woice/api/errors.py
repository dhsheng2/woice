from .response import APIJsonResponse


class ErrorResponse(APIJsonResponse):

    status_code = 500
    error_code = 5000
    error_message = ''

    def __init__(self, error=''):
        super(ErrorResponse, self).__init__(
            error=error or self.error_message,
            errno=self.error_code
        )


class InvalidParameterResponse(ErrorResponse):

    status_code = 400
    error_code = 4000

    def __init__(self, error):
        super(InvalidParameterResponse, self).__init__(
            error=error or self.error_message
        )


class MethodNotAllowedResponse(ErrorResponse):
    status_code = 405
    error_code = 4005
    error_message = 'Method not allowed'


class InvalidSignatureResponse(ErrorResponse):
    status_code = 400
    error_code = 4002
    error_message = 'Invalid signature'
