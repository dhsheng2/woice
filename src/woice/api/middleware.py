import json

from django.utils.deprecation import MiddlewareMixin
from django.conf import settings
from .user.token import AuthorizeToken, InvalidAuthorizeToken

from ..utils.logger import get_api_logger


class APIMiddleware(MiddlewareMixin):

    logger = get_api_logger()

    def __call__(self, request):
        self.logger.info(self.__class__.__name__)
        return super(APIMiddleware, self).__call__(request)


class AuthorizeTokenMiddleware(APIMiddleware):

    def __init__(self, get_response=None):
        self.get_response = get_response
        super(AuthorizeTokenMiddleware, self).__init__(get_response)

        self.white_lists = getattr(settings, 'API_ENDPOINTS_WHITE_LISTS') or []
        self.logger.info(
            'Init endpoints white list {}'.format(json.dumps(self.white_lists))
        )

    def process_response(self, request, response):
        self.logger.info(
            '{}.process_template_response for {}'.format(
                self.__class__.__name__,
                request.path
            )
        )
        self.logger.info('Append token to {}'.format(request.path))

        # Append authorize token
        if 'X-Authorize-Token'.lower() not in response:
            authorize_token = str(getattr(request, 'authorize_token', None) or '')
            response['X-Authorize-Token'] = authorize_token

        return response

    def process_request(self, request):
        token = request.META.get('HTTP_X_AUTHORIZE_TOKEN') or ''
        self.logger.info("Received authorize token '{}'".format(token))

        try:
            authorize_token = AuthorizeToken(token)
            setattr(request, 'authorize_token', authorize_token)
        except InvalidAuthorizeToken as e:
            self.logger.error(str(e))


class CommonResponseMiddleware(APIMiddleware):

    # todo
    def process_template_response(self, request, response):
        self.logger.info(self.logger.info(
            '{}.process_template_response for {}'.format(
                self.__class__.__name__,
                request.path
            )
        ))
        return response


class SignatureMiddleware(APIMiddleware):

    def process_request(self, request):
        self.logger.info("Verify signature '{}'".format(request.path))
