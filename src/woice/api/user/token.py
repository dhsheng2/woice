import time
import hashlib

from ...models.user import User


class InvalidAuthorizeToken(Exception):
    pass


def generate_user_token(user):
    """
    :param user:
    :return: str
    """
    return '{},{},{}'.format(
        user.pk,
        int(time.time()),
        hashlib.md5(user.get_salt().encode('utf-8')).hexdigest()
    )


class AuthorizeToken(object):

    EXPIRE_TTL = 43200

    RENEW_PERIOD = 3600

    user = None

    def __init__(self, token):

        try:
            parts = token.split(',')

            self.uid = int(parts[0])
            self.timestamp = int(parts[1])
            self.salt = parts[2]

        except ValueError:
            raise InvalidAuthorizeToken(
                "Invalid authorize token '{}'".format(token)
            )

        if int(time.time()) - self.timestamp >= self.EXPIRE_TTL:
            raise InvalidAuthorizeToken(
                "Authorize token '{}' has expired.".format(token)
            )

        if not self.user:
            try:
                self.user = User.objects.get(pk=self.uid)
            except User.DoesNotExist as e:
                raise InvalidAuthorizeToken(e)

        self._check_salt(self.user.get_salt())

    def _check_salt(self, raw_salt):
        if not raw_salt:
            return False
        return self.salt == hashlib.md5(raw_salt.encode('utf-8')).hexdigest()

    def need_renew(self):
        return self.timestamp - int(time.time()) > self.RENEW_PERIOD

    def renew(self):
        return generate_user_token(self.user)

    def __repr__(self):
        return self.to_string()

    def __str__(self):
        return self.to_string()

    def to_string(self):
        return '{},{},{}'.format(
            self.uid,
            self.timestamp,
            self.salt
        )

    def get_user(self):
        return self.user
