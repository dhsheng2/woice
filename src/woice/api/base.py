from django.views import View
from django.views.generic import TemplateView

from woice.utils.logger import get_api_logger
from .response import APIJsonResponse
from .errors import MethodNotAllowedResponse


class APIView(View):

    def __init__(self, **kwargs):
        super(APIView, self).__init__(**kwargs)
        self.logger = get_api_logger()

    def dispatch(self, request, *args, **kwargs):
        self.logger.info("Dispatch '{} {}'".format(request.method, request.path))
        if getattr(self, 'request', None):
            setattr(self, 'request', request)
        response = super(APIView, self).dispatch(request, *args, **kwargs)
        if not response:
            response = self.render_fallback_response()
        return response

    def http_method_not_allowed(self, request, *args, **kwargs):
        return MethodNotAllowedResponse()

    def render(self, data=None, errno=0, error='', **kwargs):
        self.logger.info("Render data for endpoint '{}'".format(self.request.path))
        if '_pretty' in self.request.GET:
            kwargs.setdefault('json_dump_params', {'indent': 4})
        return APIJsonResponse(data=data, errno=errno, error=error, **kwargs)

    def render_fallback_response(self):
        return self.render()
