from cerberus import Validator

from ..errors import InvalidParameterResponse


def verify(rules):
    names, errors = rules.keys(), dict()
    for name in names:
        if 'message' in rules[name]:
            errors[name] = rules[name].pop('message')
    validator = Validator(rules)

    def _wrapper(method):
        def _method(self, request):
            if request.method.upper() == 'GET':
                data = request.GET
            else:
                data = request.POST
            parameters = {name: data.get(name, None) for name in names}
            ok = validator.validate(parameters)
            if ok:
                return method(self, request)

            try:
                name = list(validator.errors.keys())[0]
                error = errors[name]
            except (KeyError, IndexError):
                error = "Invalid parameter '{}'".format(name)
            return InvalidParameterResponse(error)
        return _method
    return _wrapper
